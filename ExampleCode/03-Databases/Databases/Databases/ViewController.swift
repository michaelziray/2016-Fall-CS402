//
//  ViewController.swift
//  Databases
//
//  Created by Mike Z on 10/12/16.
//  Copyright © 2016 Electronic Armory. All rights reserved.
//

import UIKit
import CoreData


class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let className:String = String(describing: Student.self)
        let student:Student = NSEntityDescription.insertNewObject(forEntityName: className, into: DatabaseController.persistentContainer.viewContext) as! Student
        
        student.name = "Name"
        
        DatabaseController.saveContext()
        
        
        let fetchRequest: NSFetchRequest<Student> = Student.fetchRequest()
        
        do {
            // get results
            let searchResults = try DatabaseController.getContext().fetch(fetchRequest)
            
            print ("num of results = \(searchResults.count)")
            
            for result in searchResults as [Student] {
                print("\(result.name!)")
            }
        } catch {
            print("Error with request: \(error)")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

