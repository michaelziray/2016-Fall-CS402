//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"


class Restaurant{
    var name: String
    var location: String
    
    init(){
        name = "Restaurant"
        location = "1234.123, 123456.123"
    }
    
    init(name:String, location:String){
        self.name = name
        self.location = location
    }
    
    func description() -> String{
        return "\(name) is at \(location)"
    }
}


var bsuRestaurant = Restaurant()

var uofiRestaurant = Restaurant(name:"UofI Restaurant", location:"1,1")


for index in 0 ...  10 {
    print("\(index)")
    
}


for( var index2 = 0; index2 < 10; index2 += 2 ){
    print("\(index)")
}
