//: Playground - noun: a place where people can play

import Foundation

var string = "Hello, playground"

var count:Double = 10

count += -2

count += 1.8



string += ". How are you?"

print(string)


func checkCount(localCount:Double) -> Bool {
    if( localCount < 10 ){
        print( "less than 10")
        return true
    }
    else{
        print("greater than 10")
        return false
    }
}


checkCount( 10 as Double )
checkCount( 11.1 )
checkCount( 4.0 )

while( count < 1000 ){
    count *= 1.35
}


let 🇺🇸 = "Greatest ever!"



